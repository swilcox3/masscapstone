#include "TopController.h"
#include <iostream>
#include "Constants.h"

namespace MASS {

TopController::TopController( string path, bool runBackgroundSubtraction, int& underHood, int& overTable ) {
    try
    {
        Constants::getInstance()->setPath( path );
        cout << "Resource Path: " << Constants::getInstance()->homePath() << endl;
        //Start Python interpreter
        Py_Initialize();
        string path = Constants::getInstance()->homePath() + "pythonScripts";
        PyObject* sysPath = PySys_GetObject((char*)"path");
        PyList_Append(sysPath, PyString_FromString( path.c_str() ));
        table = new TableMotorController();
        //Give the arduino time to wake up
        sleep(2);
        //We want the table rotating while we update our background subtraction model
        if ( runBackgroundSubtraction && motorMutex.try_lock() )
        {
            table->StartMotorForward(50);
            motorMutex.unlock();
        }
        sensor = new CameraSensorController( runBackgroundSubtraction, underHood, overTable );
        Stop();        
        capture = new CaptureOneController();        
    }
    catch( ... )
    {        
        Stop();
        cout << "Could not initialize.  Check all cable connections and restart the program." << endl;
    }

}

TopController::~TopController()
{
    delete sensor;
    delete capture;
    delete table;
    Py_Finalize();
}

void TopController::StartForward()
{
    try
    {
        if ( motorMutex.try_lock())
        {
            //255 is the highest voltage we can set it to, but we have a max speed failsafe on the arduino
            table->StartMotorForward(255);
            motorMutex.unlock();
        }
    }
    catch(...)
    {        
        Stop();
        cout << "Error in start forward. Check Arduino cable connection and restart the program." << endl;
    }
}

void TopController::StartBackward()
{
    try
    {
        if ( motorMutex.try_lock() )
        {
            table->StartMotorBackward(255);
            motorMutex.unlock();
        }
    }
    catch(...)
    {        
        Stop();
        cout << "Error in start backward. Check Arduino cable connection and restart the program." << endl;
    }
}

void TopController::Stop()
{
    //lock() will block until the mutex is acquired.  This means that once any control loop releases stopMutex (which they do every iteration)
    //Stop() will grab it and hold it
    stopMutex.lock();
    //Grabbing the motorMutex means no new loops can be started
    motorMutex.lock();
    table->StopAndReset();
    //Block for a full second
    usleep(100000);
    motorMutex.unlock();
    stopMutex.unlock();
}

//Gets the next angle from webcams and runs the control loop
void TopController::MoveToNext() {
    try{
        double angle = sensor->getNextAngle( false );        
        //try_lock does not block, just returns true or false
        if ( motorMutex.try_lock() )
        {
            //We've got the motor, so start moving
            table->StartMotorForward( 100 );
        }
        else
        {
            //We can't get the motor (either because we're already moving or we're forcing a stop), so just return out
            return;
        }
        //This angle is where we start slowing down
        int rampDownAngle = 40;
        //Keep running until it's time to ramp down
        while( angle > rampDownAngle )
        {
            //See if we're forcing a stop
            if ( stopMutex.try_lock() )
                stopMutex.unlock();
            else
                break;
            angle = sensor->getNextAngle( false );
        }
        motorMutex.unlock();
        //Give our stop code a chance to grap the motorMutex
        if ( motorMutex.try_lock() )
        {            
            //This angle defines our tolerance zone for hitting our target
            int stopAngle = 3;
            table->StartMotorForward(10);
            //Move forward slowly until we hit the target
            while ( angle > stopAngle && angle < rampDownAngle )
            {
                //See if we're forcing a stop
                if ( stopMutex.try_lock() )
                    stopMutex.unlock();
                else
                    break;

                //Ignore any document under the camera until we get close
                if ( angle > 10 )
                {
                    angle = sensor->getNextAngle( false );
                }
                else
                {
                    angle = sensor->getNextAngle( true );
                }

                //Check for overshoot
                if ( angle < 360 - stopAngle && angle > rampDownAngle )
                {
                    table->StopAndReset();
                    //Give the table a chance to ramp down so we can start going backward
                    usleep(2000);
                    table->StartMotorBackward( 10 );
                    //Move backward slowly until we hit our target
                    while ( angle > stopAngle && angle < 360 - stopAngle )
                    {
                        if ( stopMutex.try_lock() )
                            stopMutex.unlock();
                        else
                            break;
                        angle = sensor->getNextAngle( true );
                    }
                    break;
                }
            }            
            motorMutex.unlock();
            table->StopAndReset();
            Capture();
        }
        else
            table->StopAndReset();
    }
    catch(...)
    {        
        Stop();
        cout << "Error in Move To Next. Check Arduino and webcam cable connections and restart the program." << endl;
    }
}

//Gets the previous angle from webcams and runs the control loop.  See above function for detailed comments.
void TopController::MoveToPrev() {
    try{        
        double angle = sensor->getPrevAngle( false );        
        if ( motorMutex.try_lock() )
        {
            table->StartMotorBackward( 100 );
        }
        else
        {
            return;
        }
        int rampDownAngle = 320;
        while( angle < rampDownAngle )
        {
            if ( stopMutex.try_lock() )
                stopMutex.unlock();
            else
                break;
            angle = sensor->getPrevAngle( false );
        }
        motorMutex.unlock();
        if ( motorMutex.try_lock() )
        {
            table->StartMotorBackward(10);
            int stopAngle = 357;
            while ( angle > rampDownAngle && angle < stopAngle )
            {
                if ( stopMutex.try_lock() )
                    stopMutex.unlock();
                else
                    break;
                if ( angle < 350 )
                    angle = sensor->getNextAngle( false );
                else
                    angle = sensor->getNextAngle( true );
                if ( angle < rampDownAngle && angle > 360 - stopAngle )
                {                    
                    table->StopAndReset();
                    usleep(2000);
                    table->StartMotorForward( 10 );
                    while ( angle > 360 - stopAngle && angle < stopAngle )
                    {
                        if ( stopMutex.try_lock() )
                            stopMutex.unlock();
                        else
                            break;
                        angle = sensor->getPrevAngle( true );
                    }
                    break;
                }
            }            
            motorMutex.unlock();
            table->StopAndReset();
            Capture();
        }
        else
            table->StopAndReset();
    }
    catch(...)
    {        
        Stop();
        cout << "Error in Move To Previous. Check Arduino and webcam cable connections and restart the program." << endl;
    }
}

//Rotate the table a given number of degrees.  Not the most accurate thing in the world
void TopController::RotateTable( double angle )
{
    try{
        if ( motorMutex.try_lock() )
        {
            table->setStartTime();
            table->Reset();
            if ( angle > 0 )
            {
                //Forward
                table->Control( angle, 0 );
            }
            else
            {
                //Backward
                table->Control( (360 - angle*-1), 360 );
            }
            motorMutex.unlock();
        }
    }
    catch(...)
    {        
        Stop();
        cout << "Error in Rotate Table. Check Arduino cable connection and restart the program." << endl;
    }
}

//Takes a picture with the imaging camera
void TopController::Capture() {
    try
    {
        if ( stopMutex.try_lock() )
        {
            capture->Capture();
            stopMutex.unlock();
        }
    }
    catch(...)
    {        
        Stop();
        cout << "Error in Capture.  Check to make sure Capture One is open and has a tethered camera connection." << endl;
    }
}

//Saves off webcam views to file
void TopController::snapWebcams( bool rectify, bool applyMasks )
{
    try
    {
        sensor->saveWebcamFrames( rectify, applyMasks );
    }
    catch(...)
    {        
        Stop();
        cout << "Error in snap webcams. Check webcam connections and restart the program." << endl;
    }
}

//Spits out the angles we find from the webcams
void TopController::FindDocuments()
{
    try
    {
        sensor->getNextAngle( true );
    }
    catch(...)
    {
        cout << "Error in find documents. Check webcam connections and restart the program." << endl;
    }
}

//Shows what the webcams see
void TopController::LiveFeed( bool rectify, bool applyMasks )
{
    try
    {
        sensor->LiveFeed( rectify, applyMasks );
    }
    catch(...)
    {
        cout << "Error in live feed. Check webcam connections and restart the program." << endl;
    }
    sensor->closeAllWindows();
}

void TopController::closeAllOpenCVWindows()
{
    sensor->closeAllWindows();
}

//Shows the live stitched feed
void TopController::showStitchedImage()
{
    sensor->displayStitchedImage();
}

//This loop listens for the buttons we have attached to the table
void TopController::runGoButtonLoop()
{
    while( shouldReadFromArduino && table->hasSerialConnection() )
    {
        string answer = table->ReadFromSerial();
        if ( answer == "g")
        {
            cout << "Forward auto button" << endl;
            MoveToNext();

        }
        else if ( answer == "b")
        {
            cout << "Backward auto button" << endl;
            MoveToPrev();

        }
        else if ( answer == "c")
        {
            cout << "Capture button" << endl;
            Capture();
        }
    }
}

//We use this to measure the vibration damping of the table
void TopController::VibrationTest()
{
    for ( int i = 0; i < 5; i++ )
    {
        table->VibrationTest();
        usleep( 5e6 );
    }

}

}


#include "CalibrateCamera.h"
#include <cstdio>
namespace MASS {
string CalibrateCamera::run(int argc, vector<string> argv, bool runFlatten, bool& quit)
{

    Settings s;
    const string inputSettingsFile = argc > 1 ? argv[1] : "default.xml";
    FileStorage fs(inputSettingsFile, FileStorage::READ); // Read the settings
    if (!fs.isOpened())
    {
        cout << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << endl;
        return "Error";
    }
    read (fs.getFirstTopLevelNode(), s);
    //fs["Settings"] >> s;
    fs.release();                                         // close Settings file

    if (!s.goodInput)
    {
        cout << "Invalid input detected. Application stopping. " << endl;
        return "Error";
    }

    vector<vector<Point2f> > imagePoints;
    Mat cameraMatrix, distCoeffs;
    Size imageSize;
    int mode = s.inputType == Settings::IMAGE_LIST ? CAPTURING : DETECTION;
    clock_t prevTimestamp = 0;
    const Scalar RED(0,0,255), GREEN(0,255,0);
    const char ESC_KEY = 27;
    bool CalibrateNow = false;
    quit = false;
    for(int i = 0;;++i)
    {
      Mat view;
      bool blinkOutput = false;

      view = s.nextImage();

      //-----  If no more image, or got enough, then stop calibration and show result -------------
      if( CalibrateNow || (mode == CAPTURING && imagePoints.size() >= (unsigned)s.nrFrames ))
      {
          if( runCalibrationAndSave(s, imageSize,  cameraMatrix, distCoeffs, imagePoints, runFlatten))
              mode = CALIBRATED;
          else
              mode = DETECTION;
      }
      if(view.empty())          // If no more images then run calibration, save and stop loop.
      {
            if( imagePoints.size() > 0 )
                runCalibrationAndSave(s, imageSize,  cameraMatrix, distCoeffs, imagePoints, runFlatten);
            break;
      }


        imageSize = view.size();  // Format input image.
        if( s.flipVertical )    flip( view, view, 0 );

        vector<Point2f> pointBuf;

        bool found;
        switch( s.calibrationPattern ) // Find feature points on the input format
        {
        case Settings::CHESSBOARD:
            found = findChessboardCorners( view, s.boardSize, pointBuf,
                CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
            break;
        case Settings::CIRCLES_GRID:
            found = findCirclesGrid( view, s.boardSize, pointBuf );
            break;
        case Settings::ASYMMETRIC_CIRCLES_GRID:
            found = findCirclesGrid( view, s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID );
            break;
        default:
            found = false;
            break;
        }

        if ( found)                // If done with success,
        {            
              // improve the found corners' coordinate accuracy for chessboard
                if( s.calibrationPattern == Settings::CHESSBOARD)
                {
                    Mat viewGray;
                    cvtColor(view, viewGray, CV_BGR2GRAY);
                    cornerSubPix( viewGray, pointBuf, Size(11,11),
                        Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
                }                
                if( mode == CAPTURING &&  // For camera only take new samples after delay time
                    (!s.inputCapture.isOpened() || ((clock() - prevTimestamp) > s.delay*1e-3*CLOCKS_PER_SEC)) )
                {                    
                    imagePoints.push_back(pointBuf);
                    prevTimestamp = clock();
                    blinkOutput = s.inputCapture.isOpened();
                }

                // Draw the corners.
                drawChessboardCorners( view, s.boardSize, Mat(pointBuf), found );
        }

        //----------------------------- Output Text ------------------------------------------------
        string msg = (mode == CAPTURING) ? "100/100" :
                      mode == CALIBRATED ? "Calibrated" : "Press 'g' to start and 'q' to quit without calibrating";

        if( mode == CAPTURING )
        {
            if(s.showUndistorsed)
                msg = format( "%d/%d Undist. Press [ESC] to finish", (int)imagePoints.size(), s.nrFrames );
            else
                msg = format( "%d/%d", (int)imagePoints.size(), s.nrFrames );
        }

        WebcamUtils::writeText( view, msg, mode == CALIBRATED ?  GREEN : RED);

        if( blinkOutput )
            bitwise_not(view, view);

        //------------------------- Video capture  output  undistorted ------------------------------
        if( mode == CALIBRATED && s.showUndistorsed )
        {
            Mat temp = view.clone();
            undistort(temp, view, cameraMatrix, distCoeffs);
            break;
        }

        //------------------------------ Show image and check for input commands -------------------
        imshow("Image View", view);
        char key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);

        if( key  == ESC_KEY )
            CalibrateNow = true;


        if( key == 'u' && mode == CALIBRATED )
           s.showUndistorsed = !s.showUndistorsed;

        if( key == 'q' )
        {
            quit = true;
            break;
        }

        if( s.inputCapture.isOpened() && key == 'g' )
        {
            mode = CAPTURING;
            imagePoints.clear();
        }
    }

    // -----------------------Show the undistorted image for the image list ------------------------
    if( s.inputType == Settings::IMAGE_LIST && s.showUndistorsed && !quit )
    {
        Mat view, rview, map1, map2;
        initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(),
            getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0),
            imageSize, CV_16SC2, map1, map2);

        for(int i = 0; i < (int)s.imageList.size(); i++ )
        {
            view = imread(s.imageList[i], 1);
            if(view.empty())
                continue;
            remap(view, rview, map1, map2, INTER_LINEAR);
            imshow("Image View", rview);
            char c = (char)waitKey();
            if( c  == ESC_KEY || c == 'q' || c == 'Q' )
                break;
        }
    }

    return s.outputFileName;
}

//! [compute_errors]
double CalibrateCamera::computeReprojectionErrors( const vector<vector<Point3f> >& objectPoints,
                                         const vector<vector<Point2f> >& imagePoints,
                                         const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                                         const Mat& cameraMatrix , const Mat& distCoeffs,
                                         vector<float>& perViewErrors)
{
    vector<Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for( i = 0; i < (int)objectPoints.size(); ++i )
    {
        projectPoints( Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,
                       distCoeffs, imagePoints2);
        err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);

        int n = (int)objectPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err*err/n);
        totalErr        += err*err;
        totalPoints     += n;
    }

    return std::sqrt(totalErr/totalPoints);
}
//! [compute_errors]
//! [board_corners]
void CalibrateCamera::calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
                                     Settings::Pattern patternType /*= Settings::CHESSBOARD*/)
{
    corners.clear();

    switch(patternType)
    {
    case Settings::CHESSBOARD:
    case Settings::CIRCLES_GRID:
        for( int i = 0; i < boardSize.height; ++i )
            for( int j = 0; j < boardSize.width; ++j )
                corners.push_back(Point3f(float( j*squareSize ), float( i*squareSize ), 0));
        break;

    case Settings::ASYMMETRIC_CIRCLES_GRID:
        for( int i = 0; i < boardSize.height; i++ )
            for( int j = 0; j < boardSize.width; j++ )
                corners.push_back(Point3f(float((2*j + i % 2)*squareSize), float(i*squareSize), 0));
        break;
    default:
        break;
    }
}
//! [board_corners]
bool CalibrateCamera::runCalibration( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
                            vector<vector<Point2f> > imagePoints, vector<Mat>& rvecs, vector<Mat>& tvecs,
                            vector<float>& reprojErrs,  double& totalAvgErr)
{

    cameraMatrix = Mat::eye(3, 3, CV_64F);
    if( s.flag & CV_CALIB_FIX_ASPECT_RATIO )
        cameraMatrix.at<double>(0,0) = 1.0;

    distCoeffs = Mat::zeros(8, 1, CV_64F);

    vector<vector<Point3f> > objectPoints(1);
    calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

    objectPoints.resize(imagePoints.size(),objectPoints[0]);

    //Find intrinsic and extrinsic camera parameters
    double rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix,
                                 distCoeffs, rvecs, tvecs, s.flag|CV_CALIB_FIX_K4|CV_CALIB_FIX_K5);

    cout << "Re-projection error reported by calibrateCamera: "<< rms << endl;

    bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                                             rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

    return ok;
}

// Print camera parameters to the output file
void CalibrateCamera::saveCameraParams( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
                              const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                              const vector<float>& reprojErrs, const vector<vector<Point2f> >& imagePoints,
                              double totalAvgErr, Mat& warpMat, Point centerPoint )
{
    std::remove( s.outputFileName.c_str() );
    FileStorage fs( s.outputFileName, FileStorage::WRITE );

    time_t tm;
    time( &tm );
    struct tm *t2 = localtime( &tm );
    char buf[1024];
    strftime( buf, sizeof(buf)-1, "%c", t2 );

    fs << "calibration_Time" << buf;

    if( !rvecs.empty() || !reprojErrs.empty() )
        fs << "nrOfFrames" << (int)std::max(rvecs.size(), reprojErrs.size());
    fs << "image_Width" << imageSize.width;
    fs << "image_Height" << imageSize.height;
    fs << "board_Width" << s.boardSize.width;
    fs << "board_Height" << s.boardSize.height;
    fs << "square_Size" << s.squareSize;

    if( s.flag & CV_CALIB_FIX_ASPECT_RATIO )
        fs << "FixAspectRatio" << s.aspectRatio;

    if( s.flag )
    {
        sprintf( buf, "flags: %s%s%s%s",
            s.flag & CV_CALIB_USE_INTRINSIC_GUESS ? " +use_intrinsic_guess" : "",
            s.flag & CV_CALIB_FIX_ASPECT_RATIO ? " +fix_aspectRatio" : "",
            s.flag & CV_CALIB_FIX_PRINCIPAL_POINT ? " +fix_principal_point" : "",
            s.flag & CV_CALIB_ZERO_TANGENT_DIST ? " +zero_tangent_dist" : "" );
        cvWriteComment( *fs, buf, 0 );

    }

    fs << "flagValue" << s.flag;

    fs << "Camera_Matrix" << cameraMatrix;
    fs << "Distortion_Coefficients" << distCoeffs;

    fs << "Avg_Reprojection_Error" << totalAvgErr;
    if( !reprojErrs.empty() )
        fs << "Per_View_Reprojection_Errors" << Mat(reprojErrs);

    if( !rvecs.empty() && !tvecs.empty() )
    {
        CV_Assert(rvecs[0].type() == tvecs[0].type());
        Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
        for( int i = 0; i < (int)rvecs.size(); i++ )
        {
            Mat r = bigmat(Range(i, i+1), Range(0,3));
            Mat t = bigmat(Range(i, i+1), Range(3,6));

            CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
            CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
            //*.t() is MatExpr (not Mat) so we can use assignment operator
            r = rvecs[i].t();
            t = tvecs[i].t();
        }
        cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0 );
        fs << "Extrinsic_Parameters" << bigmat;
    }

    if( !imagePoints.empty() )
    {
        Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
        for( int i = 0; i < (int)imagePoints.size(); i++ )
        {
            Mat r = imagePtMat.row(i).reshape(2, imagePtMat.cols);
            Mat imgpti(imagePoints[i]);
            imgpti.copyTo(r);
        }
        fs << "Image_points" << imagePtMat;
    }

    fs << "Warp_mat" << warpMat;
    fs << "Center_point" << centerPoint;
}

//! [run_and_save]
bool CalibrateCamera::runCalibrationAndSave(Settings& s, Size imageSize, Mat& cameraMatrix, Mat& distCoeffs,
                           vector<vector<Point2f> > imagePoints, bool flatten )
{
    vector<Mat> rvecs, tvecs;
    vector<float> reprojErrs;
    double totalAvgErr = 0;

    bool ok = runCalibration(s,imageSize, cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs,
                             reprojErrs, totalAvgErr);

    Mat map1;
    Mat map2;
    Mat R;
    double fx = cameraMatrix.at<double>(0);
    double cx = cameraMatrix.at<double>(2);
    double fy = cameraMatrix.at<double>(4);
    double cy = cameraMatrix.at<double>(5);
    double k1 = distCoeffs.at<double>(0);
    double k2 = distCoeffs.at<double>(1);
    double p1 = distCoeffs.at<double>(2);
    double p2 = distCoeffs.at<double>(3);
    double k3 = distCoeffs.at<double>(4);

    double cameraMatBuff[3][3] = {{fx, 0, cx}, {0, fy, cy}, {0, 0, 1}};
    double dist[5][1] = {k1, k2, p1, p2, k3};

    Mat cameraMat = Mat(3, 3, CV_64F, cameraMatBuff);

    Mat distanceCoeffs = Mat(5, 1, CV_64F, dist);

    initUndistortRectifyMap(cameraMat, distanceCoeffs, R, cameraMat, imageSize, CV_32FC1, map1, map2);

    Mat warpMat;
    s.centerPoint = Point(0, 0);
    if ( flatten )
    {
        while ( true )
        {
            warpMat = runFlatten( s, map1, map2 );
            bool good = false;
            while ( true )
            {
                Mat view = s.nextImage();
                Mat view2 = Mat(view.rows, view.cols, CV_8UC3);//may need to declare CV_8U or something
                remap(view, view2, map1, map2, INTER_LINEAR, BORDER_CONSTANT, 0);
                Mat flattened;
                warpPerspective(view2, flattened, warpMat, imageSize);
                WebcamUtils::writeText( flattened, "Confirm flattening is acceptable by pressing 'g', 'q' if not", Scalar( 0, 0, 255 ) );
                imshow("Flattened Image", flattened);
                char key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);

                if( s.inputCapture.isOpened() && key == 'g' )
                {
                    good = true;
                    break;
                }
                else if ( s.inputCapture.isOpened() && key == 'q' )
                {
                    break;
                }
            }
            if ( good )
                break;
        }
        namedWindow("Pick Centerpoint", WINDOW_AUTOSIZE);
        setMouseCallback( "Pick Centerpoint", Settings::mouseCallback, &s );
        while ( true )
        {
            Mat view = s.nextImage();
            Mat view2 = Mat(view.rows, view.cols, CV_8UC3);//may need to declare CV_8U or something
            remap(view, view2, map1, map2, INTER_LINEAR, BORDER_CONSTANT, 0);
            Mat flattened;
            warpPerspective(view2, flattened, warpMat, imageSize);
            WebcamUtils::writeText( flattened, "Click the center of the table on the image, then press 'g'", Scalar( 0, 0, 255 ) );
            if ( s.centerPoint.x != 0 && s.centerPoint.y != 0 )
            {
                circle( flattened, s.centerPoint, 5, Scalar(0, 0, 255) );
            }
            imshow("Pick Centerpoint", flattened);
            char key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);

            if( s.inputCapture.isOpened() && key == 'g' )
            {
                break;
            }
        }
    }
    else
    {
        //Identity matrix for warpPerspective (I think, we don't use it down the road anyways if we didn't do the flattening step)
        warpMat = (Mat_<float>(3,3) << 1, 1, 0, 1, 1, 0, 1, 1, 0);
        namedWindow("Pick Centerpoint", WINDOW_AUTOSIZE);
        setMouseCallback( "Pick Centerpoint", Settings::mouseCallback, &s );
        while ( true )
        {
            Mat view = s.nextImage();
            Mat view2 = Mat(view.rows, view.cols, CV_8UC3);//may need to declare CV_8U or something
            remap(view, view2, map1, map2, INTER_LINEAR, BORDER_CONSTANT, 0);
            WebcamUtils::writeText( view2, "Click the center of the table on the image, then press 'g'", Scalar( 0, 0, 255 ) );
            if ( s.centerPoint.x != 0 && s.centerPoint.y != 0 )
            {
                circle( view2, s.centerPoint, 5, Scalar(0, 0, 255) );
            }
            imshow("Pick Centerpoint", view2);
            char key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);

            if( s.inputCapture.isOpened() && key == 'g' )
            {
                break;
            }
        }
    }

    if( ok )
        saveCameraParams( s, imageSize, cameraMatrix, distCoeffs, rvecs ,tvecs, reprojErrs,
                            imagePoints, totalAvgErr, warpMat, s.centerPoint);
    destroyAllWindows();
    return ok;
}

Mat CalibrateCamera::runFlatten( Settings& s, Mat& map1, Mat& map2 )
{
    Mat inputImage = getFlattenImage( s, map1, map2 );

    vector<Point2f> undistPoints(4), sourcePoints(4);

    vector<Point2f> corners;

    findChessboardCorners(inputImage, s.boardSize, corners);

    int minx = numeric_limits<int>::max();
    int miny = numeric_limits<int>::max();
    int maxx = 0;
    int maxy = 0;

    Point TopLeft, BottomLeft, TopRight, BottomRight, center;

    center.x = 0;
    center.y = 0;

    for( int i = 0; i<corners.size(); i++)
    {
        center.x = center.x+corners[i].x;
        center.y = center.y+corners[i].y;
    }

    center.x = center.x/corners.size();
    center.y = center.y/corners.size();

    for( int i = 0; i<corners.size(); i++)
    {
        cout << "Corner: " << corners[i].x << " " << corners[i].y << endl;
        if (corners[i].x < minx && corners[i].x > 0 && corners[i].y > 0)
        {                  
            minx = corners[i].x;
        }
        if (corners[i].y < miny && corners[i].y > 0 && corners[i].y > 0)
        {            
            miny = corners[i].y;
        }
        if (corners[i].x > maxx && corners[i].x > 0 && corners[i].y > 0)
        {            
            maxx = corners[i].x;
        }
        if (corners[i].y > maxy && corners[i].x > 0 && corners[i].y > 0)
        {            
            maxy = corners[i].y;
        }
   }

    int tol = 10;
    for ( int i = 0; i < corners.size(); i++)
    {
        if ( fabs(corners[i].x - minx) < tol )
        {
            if ( fabs(corners[i].y - miny) < tol )
            {
                TopLeft = corners[i];
            }
            else if ( fabs(corners[i].y - maxy ) < tol)
            {
                BottomLeft = corners[i];
            }
        }
        else if ( fabs(corners[i].y - miny) < tol )
        {
            if ( fabs(corners[i].x - minx) < tol )
            {
                TopLeft = corners[i];
            }
            else if ( fabs(corners[i].x - maxx) < tol )
            {
                TopRight = corners[i];
            }
        }
        else if ( fabs(corners[i].x - maxx ) < tol )
        {
            if ( fabs(corners[i].y - miny ) < tol )
            {
                TopRight = corners[i];
            }
            else if ( fabs( corners[i].y - maxy) < tol )
            {
                BottomRight = corners[i];
            }
        }
        else if ( fabs(corners[i].y - maxy ) < tol )
        {
            if ( fabs(corners[i].x - minx ) < tol )
            {
                BottomLeft = corners[i];
            }
            else if (fabs(corners[i].x - maxx ) < tol )
            {
                BottomRight = corners[i];
            }
        }
    }

    double distTolerance = 45;
    undistPoints[0].x = center.x+distTolerance; //top right
    undistPoints[0].y = center.y-distTolerance;
    undistPoints[1].x = center.x-distTolerance; //top left
    undistPoints[1].y = center.y-distTolerance;
    undistPoints[2].x = center.x+distTolerance; //bottom right
    undistPoints[2].y = center.y+distTolerance;
    undistPoints[3].x = center.x-distTolerance; //bottom left
    undistPoints[3].y = center.y+distTolerance;


    sourcePoints[0] = TopRight; //top right
    sourcePoints[1] = TopLeft; //top left
    sourcePoints[2] = BottomRight; // bottom right
    sourcePoints[3] = BottomLeft; //bottom left   

    cout << "Source Points: " << sourcePoints[0] << sourcePoints[1] << sourcePoints[2] << sourcePoints[3] << endl;
    cout << "Undistorted Points: " << undistPoints[0] << undistPoints[1] << undistPoints[2] << undistPoints[3] << endl;
    return getPerspectiveTransform( sourcePoints, undistPoints );
}

Mat CalibrateCamera::getFlattenImage( Settings& s, Mat& map1, Mat& map2 )
{
    for(int i = 0;;++i)
    {
      Mat view = s.nextImage();
      Mat view2 = Mat(view.rows, view.cols, CV_8UC3);//may need to declare CV_8U or something
      remap(view, view2, map1, map2, INTER_LINEAR, BORDER_CONSTANT, 0);
      double rectHalfWidth = 70;
      double centerX = view2.size().width / 2;
      double centerY = view2.size().height / 2 - rectHalfWidth;
      Point diagonal1(centerX - rectHalfWidth, centerY - rectHalfWidth);
      Point diagonal2(centerX + rectHalfWidth, centerY + rectHalfWidth);

      rectangle( view2, diagonal1, diagonal2, Scalar(0, 0, 255 ) );
      if(view2.empty())
      {
            cout << "No image found for camera flattening" << endl;
            break;
      }

        if( s.flipVertical )    flip( view2, view2, 0 );

        //------------------------------ Show image and check for input commands -------------------
        WebcamUtils::writeText( view2, "Line up chessboard inside rectangle and then press 'g'", Scalar( 0, 0, 255 ) );
        imshow("Undistorted", view2);
        char key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);

        if( s.inputCapture.isOpened() && key == 'g' )
        {
            return view2;
        }
    }
}
}

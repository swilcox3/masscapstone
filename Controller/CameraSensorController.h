#pragma once
#include <stdio.h>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/stitching/stitcher.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include <vector>
#include <Constants.h>
#include <CalibrateCamera.h>
#include <ctime>
#include <thread>
#include <WebcamUtils.h>

namespace MASS {
    //This class handles all operations that have to do with the webcams and OpenCV
    class CameraSensorController {
    private:
        CalibrateCamera calib;
        vector<string> args1;
        vector<string> args2;
        string defaultCalibrationFile1;
        string defaultCalibrationFile2;
        Mat ROIMask1;
        Mat ROIMask2;
        VideoCapture* webcam1;
        VideoCapture* webcam2;
        Ptr<BackgroundSubtractor> pMOG;

        //Holds camera coefficients and info
        struct WebcamCoeffs
        {
            double fx;
            double cx;
            double fy;
            double cy;
            double k1;
            double k2;
            double p1;
            double p2;
            double k3;
            Mat warpMat;
            Mat map1;
            Mat map2;
            double scale;
            Point centerPoint;
        };

        WebcamCoeffs coeffs1;
        WebcamCoeffs coeffs2;

        void read(const FileStorage& node, WebcamCoeffs& coeffs);
        vector<double> documentFind( Ptr<BackgroundSubtractor> pMOG );
        Mat Rectify(Mat inputImage, WebcamCoeffs coeffs, Mat ROIMask, bool applyWarp, bool applyMask);
        void initUndistort(Size imageSize, WebcamCoeffs& coeffs);
        Mat ROI(Mat inputImage, Mat ROIMask);
        Mat cleanup(Mat inputImage);
        Mat getStitchedImage();
        void calibrateStitching( FileStorage& fs1, FileStorage& fs2 );
        void chooseWebcams( int& underHood, int& overTable );

    public:

        CameraSensorController( bool updateBackgroundSubtraction, int& underHood, int& overTable );
        ~CameraSensorController();
        Mat getWebcamFrame( VideoCapture* video );
        void displayStitchedImage();

        void saveWebcamFrames( bool rectify, bool applyMask );
        void LiveFeed( bool rectify, bool applyMask );

        void closeAllWindows();
        int calibrate();

        double getNextAngle( bool includeUnderCamera );
        double getPrevAngle( bool includeUnderCamera );
    };
}

#pragma once
#include "Constants.h"
#include <opencv2/core/core.hpp>
#include <string>
namespace MASS {
using namespace cv;
using namespace std;

class WebcamUtils {

public:
        static void writeText( Mat& view, string msg, Scalar color )
        {
            int baseLine = 0;
            Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
            Point textOrigin(view.cols - textSize.width - 10, view.rows - 2*baseLine - 10);
            putText( view, msg, textOrigin, 1, 1, color );
        }
};
}

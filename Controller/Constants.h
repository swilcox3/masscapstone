#pragma once
#include <string>

//Holds any necessary global variables
namespace MASS
{
    class Constants
    {
    private:
        std::string path;
        Constants() {}
    public:

        std::string homePath()
        {
           return path;
        }

        void setPath( std::string inPath )
        {
            path = inPath;
        }

        static Constants* getInstance()
        {
            static Constants singleton;
            return &singleton;
        }
    };
}



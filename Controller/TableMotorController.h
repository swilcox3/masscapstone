#pragma once
#include <stdio.h>
#include "Constants.h"
#include <iostream>
#include <Python.h>
#include <vector>
#include <ctime>
#include <math.h>
#include <mutex>
#include <chrono>

#define TOLERANCE 1
using namespace std;
namespace MASS {
//Handles all communication with the arduino and motor.  We use python for serial communication
class TableMotorController {
    double targetTime = 10;
    double derivative = 0;    
    double lastTablePos;
    double timestep = .033;
    double lastError = 0;
    std::vector<double> profileCoeffs;
    std::vector<double> time;
    std::vector<double> trajectory;
    std::vector<double> data;
    std::chrono::time_point<std::chrono::high_resolution_clock> startTime;
    std::chrono::time_point<std::chrono::high_resolution_clock> lastTime;
    double duration = 0;
    int iterations = 0;       
    
    PyObject* serial;
    void initSerial();
    void closeSerial();
    mutex serialMutex;

public:        
        TableMotorController()
        {
            initSerial();
        }
        ~TableMotorController() { StopAndReset(); closeSerial(); }
        PyObject* RunPythonScript( char* file, char* function, PyObject* args );
        string ReadFromSerial();
        void StartMotorForward( int voltage );
        void StartMotorBackward( int voltage );
        void UpdateProfile( double angle, double deriv, double angle_f, double deriv_f, double targetTime );
        void BeginControlForward( double angle, double deriv, double angle_f, double deriv_f, double targetTime );
        void BeginControlBackward( double angle, double deriv, double angle_f, double deriv_f, double targetTime );
        void Stop();
        void Reset();

        bool hasSerialConnection()
        {
            if ( serial )
                return true;
            else
                return false;
        }

        void setStartTime()
        {
            startTime = std::chrono::high_resolution_clock::now();
            //lastTime = std::clock();
        }
        bool Control( double angle, double targetAngle );
        void VibrationTest();
        void StopAndReset();
        void RampUp(bool);
        void RampDown(bool);
};
}

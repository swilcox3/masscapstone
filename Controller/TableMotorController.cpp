#include "TableMotorController.h"
#include <stdio.h>
#include <unistd.h>
#include <CoreFoundation/CoreFoundation.h>
namespace MASS
{
//Runs an arbitrary python script, as long as it's in the pythonScripts folder
PyObject* TableMotorController::RunPythonScript( char* scriptName, char* functionName, PyObject* pArgs )
{    
    PyObject *pName, *pModule, *pFunc, *ret;
    pName = PyString_FromString( scriptName );

    // Load the module object
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);

    if (pModule) {
        pFunc = PyObject_GetAttrString(pModule, functionName );
        if (pFunc && PyCallable_Check(pFunc)) {
            ret = PyObject_CallObject(pFunc, pArgs);
            if (!ret) {
                std::cout << "Could not run script " << scriptName << std::endl;                
            }
            Py_DECREF(pFunc);
        } else {
            std::cout << "can't call function" << std::endl;
            if (pFunc) {
                Py_DECREF(pFunc);
            }            
        }
        Py_DECREF(pModule);
        return ret;
    } else {
        PyErr_Print();
        std::cout << "can't find file" << std::endl;        
    }
}

//Close the USB connection with the arduino
void TableMotorController::closeSerial()
{
    if ( !hasSerialConnection() )
        return;
    serialMutex.lock();
    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, serial);
    PyObject* ret = RunPythonScript( "closeSerial", "Run", pArgs );
    if (!ret) {
       std::cout << "Could not close serial connection" << std::endl;
       serialMutex.unlock();
       return;
    }
    Py_DECREF(pArgs);
    Py_DECREF(ret);
    serialMutex.unlock();
}

//Open the USB connection with the arduino
void TableMotorController::initSerial()
{
   serialMutex.lock();
   PyObject* pArgs = PyTuple_New(0);
   PyObject* ret = RunPythonScript("initSerial", "Run", pArgs);
   if (!ret) {
       std::cout << "Can't initialize serial connection" << std::endl;
       serial = nullptr;
       serialMutex.unlock();
       return;
   }
   serial = ret;
   Py_DECREF(pArgs);
   serialMutex.unlock();
}

string TableMotorController::ReadFromSerial()
{
    if ( !hasSerialConnection() )
        return "";
    serialMutex.lock();
    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, serial);
    Py_INCREF( serial );
    PyObject* ret = RunPythonScript( "readSerial", "Run", pArgs );
    if (!ret) {
       std::cout << "Could not read from serial connection" << std::endl;
       Py_DECREF( ret );
       serialMutex.unlock();
       return "";
    }
    string read = PyString_AsString( ret );
    Py_DECREF( ret );
    Py_DECREF( pArgs );
    serialMutex.unlock();
    return read;
}

//Set the motor voltage directly
void TableMotorController::StartMotorForward( int voltage )
{
    if ( !hasSerialConnection() )
        return;
    serialMutex.lock();
     PyObject* pArgs = PyTuple_New(2);
     PyObject* pValue = PyInt_FromLong( voltage );
     PyTuple_SetItem(pArgs, 0, pValue);
     PyTuple_SetItem(pArgs, 1, serial);
     PyObject* ret = RunPythonScript("startMotorForward", "Run", pArgs);
     if (!ret) {
         std::cout << "Can't start motor forward" << std::endl;
         serialMutex.unlock();
         return;
     }
     Py_DECREF( pValue );
     Py_DECREF(ret);
     serialMutex.unlock();
}

void TableMotorController::StartMotorBackward( int voltage )
{
    if ( !hasSerialConnection() )
        return;
    serialMutex.lock();
     PyObject* pArgs = PyTuple_New(2);
     PyObject* pValue = PyInt_FromLong( voltage );
     PyTuple_SetItem(pArgs, 0, pValue);
     PyTuple_SetItem(pArgs, 1, serial);
     PyObject* ret = RunPythonScript("startMotorBackward", "Run", pArgs);
     if (!ret) {
         std::cout << "Can't start motor backward" << std::endl;
         serialMutex.unlock();
         return;
     }
     Py_DECREF(pValue);
     Py_DECREF(ret);
     serialMutex.unlock();
}

//Start the control loop on the arduino
void TableMotorController::BeginControlForward( double angle, double deriv, double angle_f, double deriv_f, double targetTime )
{
    if ( !hasSerialConnection() )
        return;
    serialMutex.lock();
    PyObject* pArgs = PyTuple_New(6);
    PyObject* pValue = PyFloat_FromDouble( angle );
    PyTuple_SetItem(pArgs, 0, pValue);
    pValue = PyFloat_FromDouble( deriv );
    PyTuple_SetItem(pArgs, 1, pValue);
    pValue = PyFloat_FromDouble( angle_f );
    PyTuple_SetItem(pArgs, 2, pValue);
    pValue = PyFloat_FromDouble( deriv_f );
    PyTuple_SetItem(pArgs, 3, pValue);
    pValue = PyFloat_FromDouble( targetTime );
    PyTuple_SetItem(pArgs, 4, pValue);
    PyTuple_SetItem(pArgs, 5, serial);
    PyObject* ret = RunPythonScript("beginControlForward", "Run", pArgs);
    if (!ret) {
        std::cout << "Can't begin controlling" << std::endl;
        serialMutex.unlock();
        return;
    }
    Py_DECREF(pValue);
    Py_DECREF(ret);
    serialMutex.unlock();
}

void TableMotorController::VibrationTest()
{
    if ( !hasSerialConnection() )
        return;
    serialMutex.lock();
    PyObject* pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, serial);
    RunPythonScript( "vibrationTest", "Run", pArgs );
    serialMutex.unlock();
}

void TableMotorController::BeginControlBackward( double angle, double deriv, double angle_f, double deriv_f, double targetTime )
{
    if ( !hasSerialConnection() )
        return;
    serialMutex.lock();
    PyObject* pArgs = PyTuple_New(6);
    PyObject* pValue = PyFloat_FromDouble( angle );
    PyTuple_SetItem(pArgs, 0, pValue);
    pValue = PyFloat_FromDouble( deriv );
    PyTuple_SetItem(pArgs, 1, pValue);
    pValue = PyFloat_FromDouble( angle_f );
    PyTuple_SetItem(pArgs, 2, pValue);
    pValue = PyFloat_FromDouble( deriv_f );
    PyTuple_SetItem(pArgs, 3, pValue);
    pValue = PyFloat_FromDouble( targetTime );
    PyTuple_SetItem(pArgs, 4, pValue);
    PyTuple_SetItem(pArgs, 5, serial);
    PyObject* ret = RunPythonScript("beginControlBackward", "Run", pArgs);
    if (!ret) {
        std::cout << "Can't begin controlling" << std::endl;
        serialMutex.unlock();
        return;
    }
    Py_DECREF(pValue);
    Py_DECREF(ret);
    serialMutex.unlock();
}

//Update control profile on arduino
void TableMotorController::UpdateProfile( double angle, double deriv, double angle_f, double deriv_f, double targetTime )
{
    if ( !hasSerialConnection() )
        return;
    serialMutex.lock();
    PyObject* pArgs = PyTuple_New(6);
    PyObject* pValue = PyFloat_FromDouble( angle );
    PyTuple_SetItem(pArgs, 0, pValue);
    pValue = PyFloat_FromDouble( deriv );
    PyTuple_SetItem(pArgs, 1, pValue);
    pValue = PyFloat_FromDouble( angle_f );
    PyTuple_SetItem(pArgs, 2, pValue);
    pValue = PyFloat_FromDouble( deriv_f );
    PyTuple_SetItem(pArgs, 3, pValue);
    pValue = PyFloat_FromDouble( targetTime );
    PyTuple_SetItem(pArgs, 4, pValue);
    PyTuple_SetItem(pArgs, 5, serial);
     PyObject* ret = RunPythonScript("updateProfile", "Run", pArgs);
     if (!ret) {
         std::cout << "Can't update profile" << std::endl;
         serialMutex.unlock();
         return;
     }
     Py_DECREF(pValue);
     Py_DECREF(ret);
     serialMutex.unlock();
}

void TableMotorController::Stop()
{
    if ( !hasSerialConnection() )
        return;
    serialMutex.lock();
     std::cout << "STOP" << std::endl;
     PyObject* pArgs = PyTuple_New(1);
     PyTuple_SetItem(pArgs, 0, serial);
     PyObject* ret = RunPythonScript("stopMotor", "Run", pArgs);
     if (!ret) {
        std::cout << "Could not stop the table" << std::endl;
        serialMutex.unlock();
        return;
     }
     Py_DECREF(ret);
     serialMutex.unlock();
}

bool TableMotorController::Control( double angle, double targetAngle )
{
    if ( angle < TOLERANCE ) {
        std::cout << "TARGET TIME: " << duration << " " << targetTime << std::endl;           
        return false;
    }

    if ( iterations == 0)
    {
        targetTime = fabs(targetAngle - angle) / 70;
        std::cout << "Angle: " << angle << std::endl;
        std::cout << "Derivative: " << derivative << std::endl;
        std::cout << "TargetAngle: " << targetAngle << std::endl;
        std::cout << "TargetDeriv: " << "0" << std::endl;
        std::cout << "Target Time: " << targetTime << std::endl;
        std::cout << std::endl;
        if ( angle > targetAngle )
            BeginControlForward( angle, derivative, targetAngle, 0, targetTime );
        else if ( angle < targetAngle )
            BeginControlBackward( angle, derivative, targetAngle, 0, targetTime );
        iterations++;
        lastTime = std::chrono::high_resolution_clock::now();
        lastTablePos = angle;
        //Reset(); //NOT SUPPOSED TO BE HERE IN FINAL CONTROL LOOP
        return true;
    }       

    auto stdDuration = ( std::chrono::high_resolution_clock::now() - lastTime );
    timestep = (double)chrono::duration_cast<chrono::milliseconds>(stdDuration).count() / (double)1000;
    cout << "TIMESTEP: " << timestep << endl;
    duration += timestep;
    targetTime = fabs(targetAngle - angle) / 70;
    lastTime = std::chrono::high_resolution_clock::now();
    derivative = (angle - lastTablePos) / timestep;
    lastTablePos = angle;

    std::cout << "Angle: " << angle << std::endl;
    std::cout << "Derivative: " << derivative << std::endl;
    std::cout << "TargetAngle: " << targetAngle << std::endl;
    std::cout << "TargetDeriv: " << "0" << std::endl;
    std::cout << "Target Time: " << targetTime << std::endl;
    std::cout << std::endl;
    UpdateProfile( angle, derivative, targetAngle, 0, targetTime );
    iterations++;
    return true;
}

void TableMotorController::Reset()
{
    iterations = 0;
    derivative = 0;
    lastTablePos = 0;
    duration = 0;
    targetTime = 10;
    profileCoeffs.clear();
}

void TableMotorController::StopAndReset()
{
    Stop();
    Reset();
}
}

#pragma once
#include "Constants.h"
#include <string>
namespace MASS {

//Handles any operations necessary to interface with Capture One.  Currently only runs an applescript that
//tells CaptureOne to take a picture
class CaptureOneController {
        std::string pathStr = Constants::getInstance()->homePath() + "appleScripts/myCaptureScript.sh";
        const char* path = pathStr.c_str();
public:
	void Capture();
};
}

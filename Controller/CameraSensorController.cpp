#include "CameraSensorController.h"
#include <iostream>
#include <vector>
#include <limits>
#include <math.h>
#include <unistd.h>

using namespace cv;
using namespace std;

namespace MASS {

    CameraSensorController::CameraSensorController( bool updateBackgroundSubtraction, int& underHood, int& overTable )
    {
        calib = CalibrateCamera();
        args1 = vector<string>();
        args2 = vector<string>();
        args1.push_back("in_webcam1.xml");
        args1.push_back( Constants::getInstance()->homePath() + "cameraCalibration/in_webcam1.xml" );
        args2.push_back("in_webcam2.xml");
        args2.push_back( Constants::getInstance()->homePath() + "cameraCalibration/in_webcam2.xml" );

        defaultCalibrationFile1 = Constants::getInstance()->homePath() + "cameraCalibration/out_webcam1.xml";
        defaultCalibrationFile2 = Constants::getInstance()->homePath() + "cameraCalibration/out_webcam2.xml";

        ROIMask1 = imread( Constants::getInstance()->homePath() + "masks/ROImask1.jpg", CV_LOAD_IMAGE_GRAYSCALE);
        ROIMask2 = imread( Constants::getInstance()->homePath() + "masks/ROImask2.jpg", CV_LOAD_IMAGE_GRAYSCALE);

        FileStorage fs1(defaultCalibrationFile1, FileStorage::READ);
        if (!fs1.isOpened() )
        {
            cout << "Could not open the calibration file: \"" << defaultCalibrationFile1 << "\"" << endl;
            return;
        }
        else
            read( fs1, coeffs1 );
        FileStorage fs2(defaultCalibrationFile2, FileStorage::READ);
        if (!fs2.isOpened() )
        {
            cout << "Could not open the calibration file: \"" << defaultCalibrationFile2 << "\"" << endl;
            return;
        }
        else
            read( fs2, coeffs2 );

        //OpenCV, due to a REALLY poor design choice, only allows attaching to webcams by index.  We need to save off the indices the user chooses for each webcam
        if ( underHood == -1 && overTable == -1 )
            chooseWebcams( underHood, overTable );
        webcam1 = new VideoCapture( underHood );
        if ( !webcam1 || !webcam1->isOpened() )
        {
            cout << "Could not open webcam 1" << endl;
            if ( webcam1 )
                delete webcam1;
            webcam1 = nullptr;
            return;
        }

        webcam2 = new VideoCapture( overTable );
        if ( !webcam2 || !webcam2->isOpened() )
        {
            cout << "Could not open webcam 2" << endl;
            if ( webcam2 )
                delete webcam2;
            webcam2 = nullptr;            
            return;
        }        
        initUndistort( ROIMask1.size(), coeffs1 );
        initUndistort( ROIMask2.size(), coeffs2 );        

        //Give the webcams time to wake up, do their auto-balancing, etc.
        sleep(3);

        pMOG = new BackgroundSubtractorMOG();

        if ( updateBackgroundSubtraction )
        {
            //We need to train the background subtraction model, so we take a hundred frames up front. The table must be clear.
            cout << "Initializing background subtraction model...";
            for ( int i = 0; i < 100; i++ )
            {
                Mat output;
                pMOG->operator()(getStitchedImage(), output);
            }
            cout << "Finished!" << endl;
        }

        destroyAllWindows();
    }

    CameraSensorController::~CameraSensorController()
    {
        destroyAllWindows();
        if (webcam1)
            delete webcam1;
        if (webcam2)
            delete webcam2;
    }

    //Lets the user choose what webcam index corresponds to each camera
    void CameraSensorController::chooseWebcams( int& underHood, int& overTable )
    {
        for ( int i = 0; i < 3; i++ )
        {
            VideoCapture camera = VideoCapture(i);
            namedWindow( "Choose Webcam Index", WINDOW_AUTOSIZE );
            while( true )
            {
                Mat frame = getWebcamFrame( &camera );
                WebcamUtils::writeText( frame, "If under hood press 'h', over table press 't', otherwise press 'q'", Scalar( 0, 0, 255 ) );
                imshow( "Choose Webcam Index", frame );
                char answer = waitKey( 1 );
                if ( answer == 'h' )
                {
                    underHood = i;
                    break;
                }
                if ( answer == 't' )
                {
                    overTable = i;
                    break;
                }
                if ( answer == 'q' )
                {
                    break;
                }
            }
        }
    }

    //Wrapper function to always get correctly sized images
    Mat CameraSensorController::getWebcamFrame( VideoCapture* video )
    {
        Mat frame;
        bool Success = video->read(frame);
        if (Success)
        {
            Mat scaledImage;
            //resize reduces the image resolution for faster processing
            resize( frame, scaledImage, Size(), .5, .5, INTER_AREA);
            return scaledImage;
        }
        else
            throw;
    }

    //Wrapper function for whenever we want to work on the integrated view from both webcams
    Mat CameraSensorController::getStitchedImage()
    {
        //image1 is inside the hood
        Mat image1 = Rectify( getWebcamFrame( webcam1 ), coeffs1, ROIMask1, true, false );
        //image2 is outside the hood
        Mat image2 = Rectify( getWebcamFrame( webcam2 ), coeffs2, ROIMask2, false, false );
        Mat scaled;
        resize( image1, scaled, Size(), coeffs1.scale, coeffs1.scale, INTER_AREA );
        Mat scaledROI1;
        resize( ROIMask1, scaledROI1, Size(), coeffs1.scale, coeffs1.scale, INTER_AREA );
        Mat flipped;
        //This is equivalent to rotating image1 180 degrees
        flip( scaled, flipped, -1 );
        image1 = flipped;
        Mat flippedMask;
        flip( scaledROI1, flippedMask, -1 );
        //Match up center points
        int imageXOrigin = coeffs2.centerPoint.x - (image1.cols - (coeffs1.centerPoint.x * coeffs1.scale) );
        int imageYOrigin = coeffs2.centerPoint.y - (image1.rows - (coeffs1.centerPoint.y * coeffs1.scale) );
        int imageYMax = imageYOrigin + image2.rows;
        //Make stitched image
        Mat composite = Mat::zeros( imageYMax, image2.cols, CV_8UC3 );
        image2.copyTo( composite(Rect( 0, 0, image2.cols, image2.rows )), ROIMask2 );
        image1.copyTo( composite(Rect( imageXOrigin, imageYOrigin, image1.cols, image1.rows )), flippedMask );

        return composite;
    }

    //Shows live feed of the two webcam views composited together
    void CameraSensorController::displayStitchedImage()
    {
        while ( true )
        {
            Mat stitched = getStitchedImage();
            WebcamUtils::writeText( stitched, "Press 'q' to exit" , Scalar( 0, 0, 255 ) );
            namedWindow( "Stitched", WINDOW_AUTOSIZE );
            imshow( "Stitched", stitched );
            if ( waitKey(1) == 'q' )
            {
                destroyAllWindows();
                break;
            }
        }
    }

    void CameraSensorController::saveWebcamFrames( bool rectify, bool applyMask )
    {
        if ( webcam1 )
        {
               Mat frame = getWebcamFrame( webcam1 );
               if ( rectify )
                    frame = Rectify( frame, coeffs1, ROIMask1, true, applyMask );
               cout << "Saved to: " << Constants::getInstance()->homePath() + "webcamSnapshots/webcam1.jpg" << endl;
               imwrite( Constants::getInstance()->homePath() + "webcamSnapshots/webcam1.jpg", frame);
        }

        if ( webcam2 )
        {
                Mat frame = getWebcamFrame(webcam2);
                if ( rectify )
                    frame = Rectify( frame, coeffs2, ROIMask2, false, applyMask );
                cout << "Saved to: " << Constants::getInstance()->homePath() + "webcamSnapshots/webcam2.jpg" << endl;
                imwrite( Constants::getInstance()->homePath() + "webcamSnapshots/webcam2.jpg", frame);
        }
    }

    //Opens two windows that show what each webcam sees.  Useful for sanity checks and setting up the machine
    void CameraSensorController::LiveFeed( bool rectify, bool applyMasks )
    {
        while( true )
        {
            bool showing = false;
            if ( webcam1 )
            {
                showing = true;
                Mat frame = getWebcamFrame( webcam1 );
                if ( rectify )
                    frame = Rectify( frame, coeffs1, ROIMask1, true, applyMasks );
                WebcamUtils::writeText( frame, "Press 'q' to exit", Scalar( 0, 255, 0) );
                namedWindow( "Webcam 1", WINDOW_AUTOSIZE );
                imshow( "Webcam 1", frame );
            }
            if ( webcam2 )
            {
                showing = true;
                Mat frame = getWebcamFrame( webcam2 );
                if ( rectify )
                    frame = Rectify( frame, coeffs2, ROIMask2, false, applyMasks );
                WebcamUtils::writeText( frame, "Press 'q' to exit", Scalar( 0, 255, 0) );
                namedWindow( "Webcam 2", WINDOW_AUTOSIZE );
                imshow( "Webcam 2", frame );
            }
            if ( waitKey(1) == 'q' || !showing )
            {
                destroyAllWindows();
                break;
            }
        }
        destroyAllWindows();
    }

    void CameraSensorController::closeAllWindows()
    {
        destroyAllWindows();
    }

    int CameraSensorController::calibrate()
    {        
        bool quit = false;
        string calibFile1 = calib.run( 2, args1, true, quit);
        if ( quit )
            return -1;
        FileStorage fs1(calibFile1, FileStorage::READ);
        if (!fs1.isOpened())
        {
            cout << "Could not open the calibration file: \"" << calibFile1 << "\"" << endl;
            return -1;
        }
        read(fs1, coeffs1);

        string calibFile2 = calib.run( 2, args2, false, quit);
        if ( quit )
            return -1;
        FileStorage fs2(calibFile2, FileStorage::READ);
        if (!fs2.isOpened())
        {
            cout << "Could not open the calibration file: \"" << calibFile2 << "\"" << endl;
            return -1;
        }
        read(fs2, coeffs2);
        //We need to add some stuff to the files here, because our next calibration step requires
        //access to both webcams at the same time.
        fs1.release();
        fs2.release();
        fs1 = FileStorage( calibFile1, FileStorage::APPEND);
        fs2 = FileStorage( calibFile2, FileStorage::APPEND);
        calibrateStitching( fs1, fs2 );
        return 0;
    }

    //Finds the scaling factor between webcam 1 and webcam 2
    void CameraSensorController::calibrateStitching( FileStorage& fs1, FileStorage& fs2 )
    {
        while( true )
        {
            Mat image1 = Rectify( getWebcamFrame( webcam1 ), coeffs1, ROIMask1, true, false );
            Mat image2 = Rectify( getWebcamFrame( webcam2 ), coeffs2, ROIMask2, false, false );
            Mat flipped;
            flip( image1, flipped, -1 );
            image1 = flipped;
            namedWindow( "Webcam 1", WINDOW_AUTOSIZE );
            WebcamUtils::writeText( image1, "Put board in view of both webcams and press 'g'", Scalar( 0, 0, 255 ) );
            imshow( "Webcam 1", image1 );

            namedWindow( "Webcam 2", WINDOW_AUTOSIZE );
            WebcamUtils::writeText( image2, "Put board in view of both webcams and press 'g'", Scalar( 0, 0, 255 ) );
            imshow( "Webcam 2", image2 );
            char answer = waitKey(1);
            if ( answer == 'q' )
            {
                destroyAllWindows();
                break;
            }
            else if ( answer == 'g' )
            {
                vector<Point2f> corners1;
                findChessboardCorners( image1, Size( 4, 4 ), corners1 );

                vector<Point2f> corners2;
                findChessboardCorners( image2, Size( 4, 4 ), corners2 );

                double avgDist1 = 0;
                for ( int i = 0; i < 3; i++ )
                {
                    avgDist1 += sqrt( pow( corners1[i+1].y - corners1[i].y, 2 ) + pow( corners1[i+1].x - corners1[i].x, 2 ) );

                }
                avgDist1 /= 3;


                double avgDist2= 0;
                for ( int i = 0; i < 3; i++ )
                {
                    avgDist2 += sqrt( pow( corners2[i+1].y - corners2[i].y, 2 ) + pow( corners2[i+1].x - corners2[i].x, 2 ) );
                }
                avgDist2 /= 3;

                double scaleFactor = 1;
                //Webcam 1 is closer to the table than Webcam 2, and so is always larger even if these distances don't reflect it
                if ( avgDist1 < avgDist2 )
                    scaleFactor = avgDist1 / avgDist2;
                else
                    scaleFactor = avgDist2 / avgDist1;

                //FIXME: If they skip all the calibration steps until here, the file won't be cleared and
                //we'll be adding a duplicate node.  Don't know how to overwrite a single node yet.
                coeffs1.scale = scaleFactor;
                coeffs2.scale = 1;
                fs1 << "Scale_factor" << scaleFactor;
                fs2 << "Scale_factor" << 1;

                destroyAllWindows();
                break;
            }
        }
    }

    //Read calibration constants from file
    void CameraSensorController::read(const FileStorage& node, WebcamCoeffs& coeffs)
    {
        Mat cameraMatrix;
        node["Camera_Matrix"] >> cameraMatrix;
        coeffs.fx = cameraMatrix.at<double>(0);
        coeffs.cx = cameraMatrix.at<double>(2);
        coeffs.fy = cameraMatrix.at<double>(4);
        coeffs.cy = cameraMatrix.at<double>(5);

        Mat distortionMatrix;
        node["Distortion_Coefficients"] >> distortionMatrix;
        coeffs.k1 = distortionMatrix.at<double>(0);
        coeffs.k2 = distortionMatrix.at<double>(1);
        coeffs.p1 = distortionMatrix.at<double>(2);
        coeffs.p2 = distortionMatrix.at<double>(3);
        coeffs.k3 = distortionMatrix.at<double>(4);

        node["Warp_mat"] >> coeffs.warpMat;
        //This is necessary because warpPerspective wants a specific data type
        coeffs.warpMat.convertTo( coeffs.warpMat, CV_32FC2);

        node["Center_point"] >> coeffs.centerPoint;

        FileNode scale = node["Scale_factor"];
        if ( !scale.empty() )
            scale >> coeffs.scale;
    }

    double CameraSensorController::getNextAngle( bool includeUnderCamera )
    {
        double minAngle = 360;
        vector<double> angles = documentFind( pMOG );
        for ( int i = 0; i < angles.size(); i++ )
        {            
            if (angles[i] < minAngle )
            {
                if ( includeUnderCamera || (angles[i] > 5 && angles[i] < 355 ) )
                    minAngle = angles[i];

            }
            cout << "Webcam Angle " << i << " : " << angles[i] << endl;
        }

        if (minAngle != 360)
            return minAngle;
        else
            return 0;
    }

    double CameraSensorController::getPrevAngle( bool includeUnderCamera )
    {        
        double maxAngle = 0;
        vector<double> angles = documentFind( pMOG );
        for ( int i = 0; i < angles.size(); i++ )
        {            
            if (angles[i] > maxAngle)
            {
                   if ( includeUnderCamera || ( angles[i] < 355 && angles[i] > 5 ))
                        maxAngle = angles[i];
            }
            cout << "Webcam Angle " << i << " : " << angles[i] << endl;
        }

        return maxAngle;
    }

    //Initializes undistortion maps
    void CameraSensorController::initUndistort(Size imageSize, WebcamCoeffs& coeffs)
    {
        Mat R;
        double cameraMatBuff[3][3] = {{coeffs.fx, 0, coeffs.cx}, {0, coeffs.fy, coeffs.cy}, {0, 0, 1}};
        double dist[5][1] = {coeffs.k1, coeffs.k2, coeffs.p1, coeffs.p2, coeffs.k3};

        Mat cameraMat = Mat(3, 3, CV_64F, cameraMatBuff);

        Mat distCoeffs = Mat(5, 1, CV_64F, dist);

        initUndistortRectifyMap(cameraMat, distCoeffs, R, cameraMat, imageSize, CV_32FC1, coeffs.map1, coeffs.map2);
    }

    //Applies undistortion, flattening, and ROI masks to an image
    Mat CameraSensorController::Rectify(Mat inputImage, WebcamCoeffs coeffs, Mat ROIMask, bool applyWarp, bool applyMask)
    {
        Mat output = Mat(inputImage.rows, inputImage.cols, CV_8UC3);
        remap(inputImage, output, coeffs.map1, coeffs.map2, INTER_LINEAR, BORDER_CONSTANT, 0);
        //namedWindow( "Undistorted", WINDOW_AUTOSIZE );
        //imshow("Undistorted", output);

        if ( applyWarp )
        {
            Size imageSize = cvSize(output.cols, output.rows);
            Mat unWarped = Mat(output.rows, output.cols, CV_8UC3);
            warpPerspective(output, unWarped, coeffs.warpMat, imageSize);
            //namedWindow("Unwarped", WINDOW_NORMAL);
            //imshow("Unwarped", unWarped);
            output = unWarped;
        }

        if ( applyMask )
        {
            Mat maskedImage = Mat(output.rows, output.cols, CV_32FC1);
            maskedImage = ROI(output, ROIMask);
            //namedWindow("Masked", WINDOW_NORMAL);
            //imshow("Masked", maskedImage);
            output = maskedImage;
        }
        return output;
    }

    //Discards noise in an image
    Mat CameraSensorController::cleanup( Mat newImage )
    {
        Mat closed = Mat(newImage.rows, newImage.cols, CV_8UC3);
        Mat opened = closed;

        Mat morphKernel3 = getStructuringElement(MORPH_RECT, Size(13, 13));
        Mat morphKernelbig = getStructuringElement(MORPH_RECT, Size(30, 30));

        morphologyEx(newImage, closed, MORPH_CLOSE, morphKernelbig);

        //OPEN
        morphologyEx(closed, opened, MORPH_OPEN, morphKernel3);
        return opened;
    }

    //Finds the locations of all documents on the table in degrees
    vector<double> CameraSensorController::documentFind( Ptr<BackgroundSubtractor> pMOG )
    {
        Mat newImage = getStitchedImage();
        time_t startTime = clock();

        //BACKGROUND SUBTRACTION
        Mat fgMaskMOG;
        //namedWindow("Before Subraction", WINDOW_NORMAL);
        //imshow("Before Subtraction", newImage);

        pMOG->operator()(newImage, fgMaskMOG);
        Mat processedImage = cleanup(fgMaskMOG);

        //namedWindow("After Background Subtraction", WINDOW_NORMAL);
        //imshow("After Background Subtraction", fgMaskMOG);

        //cout << "About to start finding contours: " << ( std::clock() - startTime ) / (double) CLOCKS_PER_SEC << endl;
        startTime = clock();
        //FIND CONTOURS
        vector<vector<Point> > imageContours;
        //This could be used for nested contours.  Currently we assume no nesting documents
        vector<Vec4i> hierarchy;

        findContours(processedImage, imageContours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

        //drawContours(processedImage, imageContours, 0, 255, 2, 8, hierarchy);
        vector<Point> centroids;

        //Find corners of contour
        for(int j = 0; j<imageContours.size(); j++)
        {
            int minx = numeric_limits<int>::max();
            int miny = numeric_limits<int>::max();
            int maxx = numeric_limits<int>::min();
            int maxy = numeric_limits<int>::min();

            for( int i = 0; i<imageContours[j].size(); i++)
            {
                if (imageContours[j][i].x < minx)
                {
                    minx = imageContours[j][i].x;
                }
                if (imageContours[j][i].y < miny)
                {
                    miny = imageContours[j][i].y;
                }
                if (imageContours[j][i].x > maxx)
                {
                    maxx = imageContours[j][i].x;
                }
                if (imageContours[j][i].y > maxy)
                {
                    maxy = imageContours[j][i].y;
                }
            }
            //Throw out any contours that don't lie within a reasonable range
            double distance = sqrt( pow( maxx - minx, 2 ) + pow( maxy - miny, 2 ));
            //cout << "Diagonal: " << distance << endl;
            if ( distance < 30 || distance > 600 )
                continue;

            //Calculate centroid
            centroids.push_back(Point((minx+maxx)/2, (miny+maxy)/2));
        }

        //namedWindow("ContourPaper", WINDOW_NORMAL);
        //imshow("ContourPaper", processedImage);

        //Convert pixel coordinates to angles
        Point centerPoint = coeffs2.centerPoint;
        vector<double> angles;
        for (int i = 0; i<centroids.size(); i++)
        {
            //cout << "x: " << centroids[i].x << " y: " << centroids[i].y << endl;
            //circle(processedImage, centroids[i], 5, 255);
            int y = centroids[i].y - centerPoint.y;
            int x = centroids[i].x - centerPoint.x;
            double phi = atan2(x, y);
            //Convert to degrees
            phi = phi*180/3.1415926;
            //We don't want any negative angles
            if (phi < 0)
                phi = 360 + phi;
            angles.push_back( phi );
        }

        //namedWindow("centroids", WINDOW_NORMAL);
        //imshow("centroids", processedImage);

        //cout << "DocumentFind ended: " << ( std::clock() - startTime ) / (double) CLOCKS_PER_SEC << endl;
        return angles;
    }

    //Apply region of interest mask
    Mat CameraSensorController::ROI(Mat inputImage, Mat ROImask)
    {
        Mat maskedImage = Mat(inputImage.rows, inputImage.cols, CV_32FC1, Scalar(0));
        inputImage.copyTo(maskedImage, ROImask);

        return maskedImage;
    }
}

#pragma once
#include <list>
#include <stdio.h>
#include "CameraSensorController.h"
#include "CaptureOneController.h"
#include "TableMotorController.h"
#include <Python.h>
#include <chrono>
#include <thread>
#include <mutex>

using namespace std;
namespace MASS{

class TopController {


private:	

	//Handles
        CameraSensorController* sensor;
	CaptureOneController* capture;	
	TableMotorController* table;

        //MULTITHREADING
        //motorMutex is grabbed whenever we want to run the motor
        mutex motorMutex;
        //Whenever we enter a control loop, we grap and release this mutex every iteration.  If we can't grab it, we break out.  Stop() will
        //hold on to stopMutex for a full second, killing any and all control loops
        mutex stopMutex;
        //We use this for our button listening loop.  We turn this to false when we're done listening and it will kill the loop.
        bool shouldReadFromArduino = true;

public:
	//Constructor/Destructor
        TopController(string path, bool runBackgroundSubtraction, int& underHood, int& overTable );
	~TopController();

        //Sensor functions
        void CalibrateWebcam() { sensor->calibrate(); }
        void snapWebcams( bool rectify, bool applyMask );
        void closeAllOpenCVWindows();
        void FindDocuments();
        void LiveFeed( bool rectify, bool applyMask );
        void showStitchedImage();

        //Table functions
        void runGoButtonLoop();
        void stopGoButtonLoop() { shouldReadFromArduino = false; }
	void RotateTable(double deltaAngle);
        void StartForward();
        void StartBackward();
        void Stop();	
	void MoveToNext();
        void MoveToPrev();
        void VibrationTest();

        //Camera functions
	void Capture();			
};
}

#include <Time.h>
#include <TimeLib.h>

int speedPin = 5;
int directionPin = 8;
int groundPin = 6;
int groundDirectionPin = 9;
unsigned long serialdata;
bool manualControl = true;
bool rampingUp = false;
bool rampingDown = false;
long targetVoltage = 0;
long currentVoltage = 0;
char command;
int inbyte;
int delayTime = 7;
double profile_coeffs[4];
time_t startTime;

#define MAX_SPEED 150

void setup() 
{  
  pinMode( groundPin, OUTPUT );
  pinMode( speedPin, OUTPUT ); 
  pinMode( directionPin, OUTPUT ); 
  pinMode( groundDirectionPin, OUTPUT );
  analogWrite( groundPin, 0 );
  analogWrite( speedPin, 0 );
  digitalWrite( directionPin, HIGH );
  digitalWrite( groundDirectionPin, LOW );
  profile_coeffs[0] = 0;
  profile_coeffs[1] = 0;
  profile_coeffs[2] = 0;
  profile_coeffs[3] = 0;
  Serial.begin(9600);
}

void genCoeffs( int theta_0, int theta_f, int theta_dot_0, int t_f )
{
  profile_coeffs[0] = theta_0 * 1.0;
  profile_coeffs[1] = theta_dot_0 * 1.0;
  profile_coeffs[2] = 3.0*(theta_f - theta_0) / (pow( t_f, 2 ) );
  profile_coeffs[3] = -2.0*(theta_f - theta_0) / (pow( t_f, 3 ) );
  Serial.println( profile_coeffs[0] );
  Serial.println( profile_coeffs[1] );
  Serial.println( profile_coeffs[2] );
  Serial.println( profile_coeffs[3] );
}

double gen_profile( double t )
{
  double answer = profile_coeffs[0] + profile_coeffs[1]*t + profile_coeffs[2]*pow( t, 2 ) + profile_coeffs[3] * pow( t, 3 );
  //Serial.println( answer );
  return answer;
}

void getProfile()
{
  int inputs[4];
  int i = 0;
  
  while (inbyte != ';')
  {
    serialdata = 0;
    inbyte = 0;
    while (inbyte != '/' && inbyte != ';')
    {
      inbyte = Serial.read();        
      if (inbyte > 0 && inbyte != '/' && inbyte != ';')
      { 
        serialdata = serialdata * 10 + inbyte - '0';      
      }            
    }  
    
    inputs[i] = serialdata;
    Serial.println(inputs[i]);
    i++;    
  }
  inbyte = 0;
  genCoeffs( inputs[0], inputs[1], inputs[2], inputs[3] ); 
}

long getSerial()
{ 
    serialdata = 0;
    while (inbyte != '/')
    {
      inbyte = Serial.read();  
      if (inbyte > 0 && inbyte != '/')
      { 
        serialdata = serialdata * 10 + inbyte - '0';      
      }
    }   
  
  inbyte = 0;
  Serial.println(serialdata);
  return serialdata;
}

void loop() 
{
    if ( Serial.available() > 0 )
    {
      command = Serial.read();      
      if ( command == 'f' )
      {   
        manualControl = true;
        targetVoltage = getSerial();
        if (targetVoltage > MAX_SPEED)
          targetVoltage = MAX_SPEED; 
           
        digitalWrite( directionPin, HIGH );    
        if ( currentVoltage < targetVoltage )
        {
          rampingUp = true;
          rampingDown = false;
        }
        else
        {
          rampingDown = true; 
          rampingUp = false;  
        }  
      } 
      else if ( command == 'b' ) 
      {   
        manualControl = true;
        targetVoltage = getSerial(); 
        if (targetVoltage > MAX_SPEED)
          targetVoltage = MAX_SPEED;  

        digitalWrite( directionPin, LOW );   
        if ( currentVoltage < targetVoltage )
        {
          rampingUp = true;
          rampingDown = false;
        }
        else
        {
          rampingDown = true; 
          rampingUp = false;  
        }       
      }
      else if ( command == 's' )
      {
         manualControl = true;
         targetVoltage = 0;
         rampingUp = false;
         rampingDown = true;                         
      }
      else if ( command == 'p' )
      {
        manualControl = false;
        getProfile();
        digitalWrite( directionPin, HIGH );
        
        startTime = now();
      }
      else if ( command == 'q' )
      {
        manualControl = false;  
        getProfile();      
        digitalWrite( directionPin, LOW );    
        
        startTime = now();
      }
      else if ( command == 'u' )
      {
        getProfile();
      }
      else
        command = 0;    
    } 

     if ( manualControl )
     {
        if (rampingUp)
        {
          if ( currentVoltage < targetVoltage )
          {
            currentVoltage++;
            analogWrite( speedPin, currentVoltage); 
            delay(delayTime);         
          }
          else
          {
            rampingUp = false;          
          }
        }
        else if (rampingDown)
        {
          if ( currentVoltage > targetVoltage )
          {
            currentVoltage--;
            analogWrite( speedPin, currentVoltage);          
            delay(delayTime);
          }
          else
          {
            rampingDown = false;
          }        
        }      
     }
     else
     {
       double profileValue = gen_profile( second( now() - startTime ) );
       int voltage = 2 * profileValue;
       analogWrite( speedPin, voltage );
       delay( delayTime );     
     }
     
}


/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   32,   32,   32, 0x08,
      33,   32,   32,   32, 0x08,
      51,   32,   32,   32, 0x08,
      71,   32,   32,   32, 0x08,
      94,   32,   32,   32, 0x08,
     116,   32,   32,   32, 0x08,
     139,   32,   32,   32, 0x08,
     160,   32,   32,   32, 0x08,
     182,   32,   32,   32, 0x08,
     204,   32,   32,   32, 0x08,
     227,   32,   32,   32, 0x08,
     251,   32,   32,   32, 0x08,
     278,   32,   32,   32, 0x08,
     302,   32,   32,   32, 0x08,
     327,  348,   32,   32, 0x08,
     356,  348,   32,   32, 0x08,
     381,   32,   32,   32, 0x08,
     406,   32,   32,   32, 0x08,
     432,   32,   32,   32, 0x08,
     459,   32,   32,   32, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0on_capture_clicked()\0\0"
    "on_stop_clicked()\0on_rotate_clicked()\0"
    "on_capture_2_clicked()\0on_return_2_clicked()\0"
    "on_calibrate_clicked()\0on_forward_pressed()\0"
    "on_forward_released()\0on_backward_pressed()\0"
    "on_backward_released()\0on_webcamSnap_clicked()\0"
    "on_findDocuments_clicked()\0"
    "on_liveWebcam_clicked()\0"
    "on_closeOpenCV_clicked()\0on_ROI_clicked(bool)\0"
    "checked\0on_rectify_clicked(bool)\0"
    "on_stitchImage_clicked()\0"
    "on_forwardAngle_clicked()\0"
    "on_BackwardAngle_clicked()\0"
    "on_vibration_clicked()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_capture_clicked(); break;
        case 1: _t->on_stop_clicked(); break;
        case 2: _t->on_rotate_clicked(); break;
        case 3: _t->on_capture_2_clicked(); break;
        case 4: _t->on_return_2_clicked(); break;
        case 5: _t->on_calibrate_clicked(); break;
        case 6: _t->on_forward_pressed(); break;
        case 7: _t->on_forward_released(); break;
        case 8: _t->on_backward_pressed(); break;
        case 9: _t->on_backward_released(); break;
        case 10: _t->on_webcamSnap_clicked(); break;
        case 11: _t->on_findDocuments_clicked(); break;
        case 12: _t->on_liveWebcam_clicked(); break;
        case 13: _t->on_closeOpenCV_clicked(); break;
        case 14: _t->on_ROI_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->on_rectify_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->on_stitchImage_clicked(); break;
        case 17: _t->on_forwardAngle_clicked(); break;
        case 18: _t->on_BackwardAngle_clicked(); break;
        case 19: _t->on_vibration_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QPushButton *stop;
    QTabWidget *tabWidget;
    QWidget *manual;
    QGridLayout *gridLayout_3;
    QPushButton *forward;
    QPushButton *capture;
    QPushButton *backward;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_2;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *forwardAngle;
    QPushButton *BackwardAngle;
    QLineEdit *angleEdit;
    QWidget *semiAuto;
    QGridLayout *gridLayout_2;
    QPushButton *rotate;
    QPushButton *return_2;
    QPushButton *capture_2;
    QWidget *debug;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QPushButton *webcamSnap;
    QPushButton *liveWebcam;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QCheckBox *rectify;
    QCheckBox *ROI;
    QPushButton *findDocuments;
    QPushButton *calibrate;
    QPushButton *closeOpenCV;
    QPushButton *stitchImage;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(283, 475);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        stop = new QPushButton(centralWidget);
        stop->setObjectName(QString::fromUtf8("stop"));

        gridLayout->addWidget(stop, 2, 0, 1, 1);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        manual = new QWidget();
        manual->setObjectName(QString::fromUtf8("manual"));
        gridLayout_3 = new QGridLayout(manual);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        forward = new QPushButton(manual);
        forward->setObjectName(QString::fromUtf8("forward"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(forward->sizePolicy().hasHeightForWidth());
        forward->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(forward, 0, 0, 1, 1);

        capture = new QPushButton(manual);
        capture->setObjectName(QString::fromUtf8("capture"));

        gridLayout_3->addWidget(capture, 3, 0, 1, 1);

        backward = new QPushButton(manual);
        backward->setObjectName(QString::fromUtf8("backward"));

        gridLayout_3->addWidget(backward, 1, 0, 1, 1);

        widget_3 = new QWidget(manual);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        sizePolicy.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
        widget_3->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(widget_3);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        widget_2 = new QWidget(widget_3);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        sizePolicy.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy);
        verticalLayout_2 = new QVBoxLayout(widget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        forwardAngle = new QPushButton(widget_2);
        forwardAngle->setObjectName(QString::fromUtf8("forwardAngle"));

        verticalLayout_2->addWidget(forwardAngle);

        BackwardAngle = new QPushButton(widget_2);
        BackwardAngle->setObjectName(QString::fromUtf8("BackwardAngle"));

        verticalLayout_2->addWidget(BackwardAngle);


        horizontalLayout_2->addWidget(widget_2);

        angleEdit = new QLineEdit(widget_3);
        angleEdit->setObjectName(QString::fromUtf8("angleEdit"));

        horizontalLayout_2->addWidget(angleEdit);


        gridLayout_3->addWidget(widget_3, 2, 0, 1, 1);

        tabWidget->addTab(manual, QString());
        semiAuto = new QWidget();
        semiAuto->setObjectName(QString::fromUtf8("semiAuto"));
        gridLayout_2 = new QGridLayout(semiAuto);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        rotate = new QPushButton(semiAuto);
        rotate->setObjectName(QString::fromUtf8("rotate"));

        gridLayout_2->addWidget(rotate, 0, 0, 1, 1);

        return_2 = new QPushButton(semiAuto);
        return_2->setObjectName(QString::fromUtf8("return_2"));

        gridLayout_2->addWidget(return_2, 2, 0, 1, 1);

        capture_2 = new QPushButton(semiAuto);
        capture_2->setObjectName(QString::fromUtf8("capture_2"));

        gridLayout_2->addWidget(capture_2, 3, 0, 1, 1);

        tabWidget->addTab(semiAuto, QString());
        debug = new QWidget();
        debug->setObjectName(QString::fromUtf8("debug"));
        gridLayout_4 = new QGridLayout(debug);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        groupBox = new QGroupBox(debug);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        webcamSnap = new QPushButton(groupBox);
        webcamSnap->setObjectName(QString::fromUtf8("webcamSnap"));

        verticalLayout->addWidget(webcamSnap);

        liveWebcam = new QPushButton(groupBox);
        liveWebcam->setObjectName(QString::fromUtf8("liveWebcam"));

        verticalLayout->addWidget(liveWebcam);

        widget = new QWidget(groupBox);
        widget->setObjectName(QString::fromUtf8("widget"));
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        rectify = new QCheckBox(widget);
        rectify->setObjectName(QString::fromUtf8("rectify"));

        horizontalLayout->addWidget(rectify);

        ROI = new QCheckBox(widget);
        ROI->setObjectName(QString::fromUtf8("ROI"));
        ROI->setEnabled(false);
        ROI->setCheckable(true);

        horizontalLayout->addWidget(ROI);


        verticalLayout->addWidget(widget);


        gridLayout_4->addWidget(groupBox, 2, 0, 1, 1);

        findDocuments = new QPushButton(debug);
        findDocuments->setObjectName(QString::fromUtf8("findDocuments"));

        gridLayout_4->addWidget(findDocuments, 0, 0, 1, 1);

        calibrate = new QPushButton(debug);
        calibrate->setObjectName(QString::fromUtf8("calibrate"));

        gridLayout_4->addWidget(calibrate, 1, 0, 1, 1);

        closeOpenCV = new QPushButton(debug);
        closeOpenCV->setObjectName(QString::fromUtf8("closeOpenCV"));
        sizePolicy1.setHeightForWidth(closeOpenCV->sizePolicy().hasHeightForWidth());
        closeOpenCV->setSizePolicy(sizePolicy1);

        gridLayout_4->addWidget(closeOpenCV, 4, 0, 1, 1);

        stitchImage = new QPushButton(debug);
        stitchImage->setObjectName(QString::fromUtf8("stitchImage"));

        gridLayout_4->addWidget(stitchImage, 5, 0, 1, 1);

        tabWidget->addTab(debug, QString());

        gridLayout->addWidget(tabWidget, 1, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 283, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MASS", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        stop->setWhatsThis(QApplication::translate("MainWindow", "Stop the table immediately", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        stop->setText(QApplication::translate("MainWindow", "Stop", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        forward->setWhatsThis(QApplication::translate("MainWindow", "Rotate the table forwards", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        forward->setText(QApplication::translate("MainWindow", "Forward", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        capture->setWhatsThis(QApplication::translate("MainWindow", "Take a picture", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        capture->setText(QApplication::translate("MainWindow", "Capture", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        backward->setWhatsThis(QApplication::translate("MainWindow", "Rotate the table backwards", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        backward->setText(QApplication::translate("MainWindow", "Backward", 0, QApplication::UnicodeUTF8));
        forwardAngle->setText(QApplication::translate("MainWindow", "Forward", 0, QApplication::UnicodeUTF8));
        BackwardAngle->setText(QApplication::translate("MainWindow", "Backward", 0, QApplication::UnicodeUTF8));
        angleEdit->setInputMask(QApplication::translate("MainWindow", "999", 0, QApplication::UnicodeUTF8));
        angleEdit->setText(QApplication::translate("MainWindow", "120", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(manual), QApplication::translate("MainWindow", "Manual", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        rotate->setWhatsThis(QApplication::translate("MainWindow", "Move to table until the next item is centered under the camera", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        rotate->setText(QApplication::translate("MainWindow", "Rotate to Next", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        return_2->setWhatsThis(QApplication::translate("MainWindow", "Reverses the table motion until the last document captured is centered under the camera", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        return_2->setText(QApplication::translate("MainWindow", "Return to Last", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        capture_2->setWhatsThis(QApplication::translate("MainWindow", "Take a picture", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        capture_2->setText(QApplication::translate("MainWindow", "Capture", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(semiAuto), QApplication::translate("MainWindow", "Semi Auto", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", "Webcam Views", 0, QApplication::UnicodeUTF8));
        webcamSnap->setText(QApplication::translate("MainWindow", "Webcam Snapshots", 0, QApplication::UnicodeUTF8));
        liveWebcam->setText(QApplication::translate("MainWindow", "Live Webcam Feed", 0, QApplication::UnicodeUTF8));
        rectify->setText(QApplication::translate("MainWindow", "Rectify", 0, QApplication::UnicodeUTF8));
        ROI->setText(QApplication::translate("MainWindow", "ROI", 0, QApplication::UnicodeUTF8));
        findDocuments->setText(QApplication::translate("MainWindow", "Find Documents", 0, QApplication::UnicodeUTF8));
        calibrate->setText(QApplication::translate("MainWindow", "Calibrate Webcams", 0, QApplication::UnicodeUTF8));
        closeOpenCV->setText(QApplication::translate("MainWindow", "Close OpenCV Windows", 0, QApplication::UnicodeUTF8));
        stitchImage->setText(QApplication::translate("MainWindow", "Display Stitched Image", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(debug), QApplication::translate("MainWindow", "Debug", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

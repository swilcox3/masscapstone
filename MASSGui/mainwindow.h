#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <TopController.h>
#include <thread>


namespace Ui {
class MainWindow;
}

#define THREAD_POOL_SIZE 10

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_capture_clicked();    

    void on_stop_clicked();

    void on_rotate_clicked();

    void on_capture_2_clicked();

    void on_return_2_clicked();

    void on_calibrate_clicked();

    void on_forward_pressed();

    void on_forward_released();

    void on_backward_pressed();

    void on_backward_released();

    void on_webcamSnap_clicked();
    
    void on_findDocuments_clicked();

    void on_liveWebcam_clicked();

    void on_closeOpenCV_clicked();    

    void on_ROI_clicked(bool checked);

    void on_rectify_clicked(bool checked);

    void on_stitchImage_clicked();

    void on_forwardAngle_clicked();

    void on_BackwardAngle_clicked();

    void on_vibration_clicked();

private:
    //Get the next empty slot we can assign a thread to
    int getThreadIndex()
    {
        if (threadPool[threadIndex].joinable())
            threadPool[threadIndex].join();
        int currentThreadIndex = threadIndex;
        threadIndex++;
        if (threadIndex > THREAD_POOL_SIZE - 1)
            threadIndex = 0;
        return currentThreadIndex;
    }
    
    void joinThreads()
    {
        for ( int i = 0; i < THREAD_POOL_SIZE; i++ )
        {
            if (threadPool[i].joinable())
                threadPool[i].join();
        }
        if ( messageLoop.joinable() )
            messageLoop.join();
    }
    
    Ui::MainWindow *ui;
    MASS::TopController* controller;
    std::thread threadPool[ THREAD_POOL_SIZE ];
    std::thread messageLoop;
    int threadIndex = 0;
    bool mROI = false;
    bool mRectify = false;
    
};

#endif // MAINWINDOW_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <fstream>
#include <QFileDialog.h>
#include <QMessageBox.h>
#include <QShortcut>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ifstream configFile;
    configFile.open( "MASSConfig.txt" );
    string dir;
    int underHood = -1;
    int overTable = -1;
    if ( !configFile.is_open() )
    {
        //Choose where the resource directory is
        dir = QFileDialog::getExistingDirectory(this, tr("Choose Resource Directory"), "~/", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks).toStdString();
        dir = dir + "/";
        ofstream outConfig( "MASSConfig.txt" );
        outConfig << dir << endl;

        controller = new MASS::TopController( dir, false, underHood, overTable );
        outConfig << underHood << endl;
        outConfig << overTable << endl;
        outConfig.close();
    }
    else
    {
        try
        {
            getline( configFile, dir );
            string underHoodString;
            string overTableString;
            getline( configFile, underHoodString );
            getline( configFile, overTableString );
            underHood = stoi( underHoodString );
            overTable = stoi( overTableString );
            configFile.close();
        }
        catch( ... )
        {
            cout << "Error reading MASSConfig.txt" << endl;
        }
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Background Subtraction", "In order for the machine to operate successfully the background subtraction model must be updated.  The table must be completely clear before starting. Are you ready to begin?", QMessageBox::No | QMessageBox::Yes );

        bool updateBackgroundSubtraction = true;
        if( reply == QMessageBox::No )
            updateBackgroundSubtraction = false;
        controller = new MASS::TopController( dir, updateBackgroundSubtraction, underHood, overTable );
    }

    messageLoop = std::thread([this]() {
        controller->runGoButtonLoop();
    });
    ui->setupUi(this);
    QShortcut* shortcut = new QShortcut( QKeySequence("Space"), this );
    QObject::connect( shortcut, SIGNAL(activated()), this, SLOT(on_stop_clicked() ) );
}

MainWindow::~MainWindow()
{
    controller->stopGoButtonLoop();
    joinThreads();
    delete controller;
    delete ui;
}

void MainWindow::on_capture_clicked()
{
    controller->Capture();
}

void MainWindow::on_stop_clicked()
{    
    threadPool[getThreadIndex()] = std::thread([this]() {
        controller->Stop();
    });
    controller->stopGoButtonLoop();
    joinThreads();
    messageLoop = std::thread([this]() {
        controller->runGoButtonLoop();
    });
}

void MainWindow::on_rotate_clicked()
{
    threadPool[getThreadIndex()] = std::thread([this](){
        controller->MoveToNext();
    });
}

void MainWindow::on_capture_2_clicked()
{
    controller->Capture();
}

void MainWindow::on_return_2_clicked()
{
    threadPool[getThreadIndex()] = std::thread([this](){
        controller->MoveToPrev();
    });
}

void MainWindow::on_calibrate_clicked()
{
    controller->CalibrateWebcam();
}

void MainWindow::on_forward_pressed()
{
    controller->StartForward();
}

void MainWindow::on_forward_released()
{
    controller->Stop();
}

void MainWindow::on_backward_pressed()
{
    controller->StartBackward();
}

void MainWindow::on_backward_released()
{
    controller->Stop();
}

void MainWindow::on_webcamSnap_clicked()
{
    controller->snapWebcams( mRectify, mROI );
}

void MainWindow::on_findDocuments_clicked()
{
    controller->FindDocuments();
}

void MainWindow::on_liveWebcam_clicked()
{
    controller->LiveFeed( mRectify, mROI );
}

void MainWindow::on_closeOpenCV_clicked()
{
    controller->closeAllOpenCVWindows();
}

void MainWindow::on_ROI_clicked(bool checked)
{
    mROI = checked;
}

void MainWindow::on_rectify_clicked(bool checked)
{
    mRectify = checked;
    ui->ROI->setEnabled( checked );
}

void MainWindow::on_stitchImage_clicked()
{
    controller->showStitchedImage();
}

void MainWindow::on_forwardAngle_clicked()
{
    controller->RotateTable( ui->angleEdit->text().toInt() );
}

void MainWindow::on_BackwardAngle_clicked()
{
    controller->RotateTable( -1 * ui->angleEdit->text().toInt() );
}

void MainWindow::on_vibration_clicked()
{
    controller->VibrationTest();
}
